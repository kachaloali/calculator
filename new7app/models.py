# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

def is_even(nbr):
    return nbr % 2 == 0

def square(x):
	return x*x
