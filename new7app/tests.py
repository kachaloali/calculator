# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

# Create your tests here.
#import unittest
from new7app.models import *
class mytest(TestCase):	
    def test_is_even(self):
        self.assertTrue(is_even(2))
        self.assertFalse(is_even(1))
        self.assertEqual(is_even(0), True)
       
    def test_square(self):
        self.assertEqual(square(2), 4)

#if __name__ == '__main__':
#	unittest.main()
